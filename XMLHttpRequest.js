/*
const request = new XMLHttpRequest();

request.open('GET', './persona.json', true);

request.addEventListener('load', () => {
	if (request.status === 200) {
		console.log(JSON.parse(request.response));
	} else  {
		console.log(request.status, request.statusText);
	}
});

request.addEventListener('error', () => {
	console.log("Algun error de la red");
});

request.send(null);
*/


function getReq (url) {
	const miPromesa = new Promise(function(resolve, reject) {
		const request = new XMLHttpRequest();

		request.open('GET', url, true);

		request.addEventListener('load', () => {
			if (request.status === 200) {
				resolve(request.response);
			} else  {
				reject(request.statusText);
			}
		});

		request.addEventListener('error', () => {
			reject("Algun error de la red");
		});

		request.send(null);
	});

	return miPromesa;
}


getReq('./persona.json')
.then(res => {
	return JSON.parse(res);
})
.then(json => {
	console.log(json);
})
.catch(error => {
	console.log(error);
})
